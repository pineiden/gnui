# import pdb

from tkinter import Tk, Toplevel, Menu, Frame, FALSE, END, Text, StringVar, N, S, E, W, RIGHT, Y, NSEW, CENTER, Button
from tkinter import ttk
import tkinter as tk
from settings import USER_COLOR, \
    WELCOME_MSG, WCM_TAG, IS_DEV, TABS, \
    BASE_GROUP, WIDTHS, UI_TITLE, \
    MAIN_MENU

# Import GNCSocket

from gus.gnc_socket import GNCSocket

from gns.library import pattern_value, fill_pattern, context_split

# from gus.scheduler import Scheduler, ReadWait, WriteWait

from gnch.gnch_settings import LIST_RECV

try:
    from hard_work import workers, set_executor
except Exception:
    from .hard_work import workers, set_executor

try:
    from .tkevents import TkEventLoop
except Exception:
    from tkevents import TkEventLoop

# Multiprocess
# import multiprocessing
import concurrent.futures
import threading
import asyncio

from multiprocessing import Pool, Process, Pipe

async def async_ui(func, *args):
    await asyncio.new_event_loop().run_in_executor(None, func, *args)


class GPSNetworkUI(tk.Frame):

    # Define methods
    def __init__(self, nproc_f=40, *args, master=None, **kwargs):
        tk.Frame.__init__(self, master,  width=800, height=400)
        # Socket
        self.tasks = []
        self.mode = 'client'
        self.btn_connect = 'OFF'
        self.group = BASE_GROUP
        self.tag = "user:" + self.group
        self.widths = WIDTHS
        self.init_app()
        self.define_main_menu()
        self.app_tabs()
        self.build_tabs()
        self.msg_entry()
        self.info_column()
        self.close_protocol()
        self.factor = nproc_f  # X%
        self.wk = workers(self.factor)
        self.executor = set_executor(self.wk)
        # Enable Pool for workers
        self.loop = asyncio.new_event_loop()
        self.tasks = []
        self.gus = GNCSocket(mode=self.mode)
        self.parent_pipe, self.child_pipe = Pipe()
        self.parent_pipe_msg, self.child_pipe_msg = Pipe()

    async def connect(self):
        self.print_msg(self.btn_connect)
        if self.gus.status == 'OFF':
            print("Conectado a GNC Socket como cliente")
            self.print_msg("Conectado a GNC Socket como cliente")
            self.print_msg("Comunicando a GUS<->GNC<->GNS")
            self.conn_btn["text"] = "Conectado a GNS"
            self.conn_btn["fg"] = "green"
            await self.connect_sock()
            # await self.gus.send_msg("Holla")
            self.print_msg("Socket en " + self.gus.gnc_path)
            self.print_msg("Socket en " + self.gus.status)
            # Must receive a Welcome message
            # await self.gus.recv_msg()
            await self.communicate()
            await asyncio.sleep(0.5)
            self.btn_connect = 'ON'
            self.print_msg(self.gus.msg_r)
            self.parent_pipe_msg.send("OK")
            # self.parent_pipe.close()

            #"await self.infoloop()
            # await async_ui(self.infoloop)
            # result = fut_info.result()
            # await self.infoloop()
        elif self.gus.status == 'ON':
            self.print_msg("Desconectando GNC Socket como cliente")
            self.conn_btn["text"] = "Conectar a GNS"
            self.conn_btn["fg"] = "red"
            self.btn_connect = 'OFF'
            print(self.gus.status)
            self.print_msg(self.gus.status + ":" + self.btn_connect)
            await self.gus.send_msg("DONE")
            self.gus.close()
            self.gus = GNCSocket(mode=self.mode)
            self.print_msg(self.gus.status)
            self.parent_pipe_msg.send("NO")
            self.print_msg(20 * ":")

    async def connect_sock(self):
        try:
            await self.gus.connect()
            # await self.communicate()
        except:
            print("No se encuentra socket")
        # connect_task = asyncio.wait_for(self.gus.connect(), None)
        # self.tasks.append(connect_task)
        # comm_task = asyncio.ensure_future(self.communicate())
        # self.tasks.append(comm_task)
        # print("Comunicando")

    async def communicate(self):
        try:
            # await this
            print("Socket GUS")
            datagram = await self.gus.recv_msg()
            # self.print_msg(self.gs.msg_r)
            print("Cierre recoleccion")
        except:
            self.print_msg("Error en recepcion de datos")

    # from entry text
    def sendmsg(self, event):
        this_msg = self.msg_entry.get()
        print(self.msg)
        print(this_msg)
        var_list = []
        if IS_DEV:
            print("Se envía mensaje")
            print(this_msg)
        struct = LIST_RECV['INFO']['code']
        # print(struct)
        name = '[MSG_DATA]'
        var_list.append(pattern_value('[CLIENTID]', 'ANNON'))
        var_list.append(pattern_value(name, "\"" + this_msg + "\""))
        print(var_list)
        msg = fill_pattern(var_list, struct)
        self.print_msg(msg)
        if self.gus.status == 'ON' and self.btn_connect == 'ON':
            self.loop.run_until_complete(self.send_msg(msg))
            self.parent_pipe_msg.send("OK")
            # msg_entry.set(StringVar())
            # text.insert(END, "\n" + msg.get())
            self.print_msg(this_msg)
        else:
            self.msg_entry.delete(0, END)
        msg_input = self.child_pipe.recv()
        self.print_msg(msg_input)

    async def send_msg(self, msg):
        await self.gus.send_msg(msg)

    def init_app(self):
        self.root = Tk()
        # modern menus
        self.root.option_add('*tearOff', FALSE)
        self.win = Toplevel(self.root)
        win = self.win
        win.title(UI_TITLE)
        win.resizable(True, True)
        win.iconify()
        self.root.withdraw()

    def define_main_menu(self):
        # Define menus
        win = self.win
        self.menubar = Menu(win)
        menubar = self.menubar
        win["menu"] = menubar
        # Declare menu list
        menu_file = Menu(menubar)
        menu_edit = Menu(menubar)
        helpmenu = Menu(menubar, name='help')

        menubar.add_cascade(
            menu=menu_file, label=MAIN_MENU['m_file']['name'], underline=0)
        menubar.add_cascade(
            menu=menu_edit, label=MAIN_MENU['m_edit']['name'], underline=0)
        menubar.add_cascade(
            menu=helpmenu, label=MAIN_MENU['m_help']['name'], underline=0)

        # Define every menu
        # menu_file
        for m in MAIN_MENU['m_file']['values']:
            # mainmenu(name_menu,option)
            menu_file.add_command(label=m, command='')

    def app_tabs(self):
        # Define Notebook tabs
        self.notebook = ttk.Notebook(self.win)
        notebook = self.notebook
        TAB = dict()
        for tab in TABS:
            TAB[tab] = ttk.Frame(notebook)
            notebook.add(TAB[tab], text=tab)

        self.TAB = TAB

        notebook.grid(column=0, row=1)

    def build_tabs(self):
        # Consola activa
        chat_now = self.TAB[TABS[0]]
        text_frame = ttk.Frame(chat_now)
        text_frame.grid(column=0, row=0)
        # Information frame:
        # scrollbar
        scroll_text = ttk.Scrollbar(text_frame)
        scroll_text.pack(side=RIGHT, fill=Y)
        text = Text(
            text_frame,
            width=60,
            height=50,
            cursor='cross',
            yscrollcommand=scroll_text.set)
        self.text = text
        text.insert(END, WELCOME_MSG, (WCM_TAG))
        text.tag_configure(
            WCM_TAG,
            foreground=USER_COLOR['user'][
                'fg'], font='times 18 bold italic',
            relief='groove', justify='center')
        text.config(state='disabled')
        text.pack()
        scroll_text.config(command=text.yview)

    def info_column(self):
        ###############################################
        # Info column
        ###############################################
        chat_now = self.TAB[TABS[0]]
        text = self.text
        color_code_bg = "gainsboro"
        info_frame = Frame(chat_now, bg=color_code_bg, width=self.widths[1])
        info_frame.grid(row=0, column=1, sticky=N)
        # Connect to server
        # Create button
        # Values are in settings
        conn_btn = Button(
            info_frame, text='Connectar\n a\n GNS', fg='red', command='')
        # COMANDO CONNECT
        conn_btn["command"] = lambda: asyncio.ensure_future(
            self.connect())
        self.conn_btn = conn_btn
        # conn_btn.config(justify='CENTER')
        conn_btn.grid(pady=20)
        # Text settings
        user_label = dict()
        square_label = dict()

        color_code = ttk.Label(
            info_frame,
            text="Código de colores",
            background=color_code_bg,
            font='times 14 bold')
        color_code.grid(column=0, row=1)
        count = 2
        for group in USER_COLOR.keys():
            # Create label
            user_label[group] = ttk.Label(
                info_frame,
                text=group,
                width=self.widths[1] - 1,
                background=color_code_bg)
            square_label[group] = ttk.Label(
                info_frame,
                text=" ",
                background=USER_COLOR[group]['fg'], width=1)
            user_label[group].grid(column=0, row=count)
            square_label[group].grid(column=1, row=count)
            count += 1
            text.tag_configure(
                'user:' + group,
                foreground=USER_COLOR[group]['fg'],
                font=USER_COLOR[group]['font'],
                relief=USER_COLOR[group]['relief'])

    def msg_entry(self):
        # msg entry
        self.msg = StringVar()
        chat_now = self.TAB[TABS[0]]
        msg_entry = ttk.Entry(
            chat_now,
            width=self.widths[0],
            cursor='xterm',
            textvariable=self.msg)
        self.msg_entry = msg_entry
        # First return
        msg_entry.bind("<Return>", self.sendmsg)
        # Numeric pad return
        msg_entry.bind("<KP_Enter>", self.sendmsg)
        msg_entry.grid(row=1, column=0, pady=2)
        send_btn = ttk.Button(
            chat_now, text="Enviar", width=self.widths[1], underline=0)
        # Mouse first button
        send_btn.bind("<Button-1>", self.sendmsg)
        send_btn.grid(row=1, column=1, padx=4)

    def close_protocol(self):
        self.win.protocol("WM_DELETE_WINDOW", self.on_closing)

    def on_closing(self):
        print("Se cierra la terminal GPS Network")
        self.loop.close()
        self.root.destroy()

    def mainloop(self):
        self.root.mainloop()

    def msg_input(self):
        while True:
            msg = self.child_pipe_msg.recv()
            self.print_msg(msg)

    def print_msg(self, msg):
        text = self.text
        text.config(state='normal')
        text.insert(END, "\n" + msg, (self.tag))
        text.config(state='disabled')
        text.yview_pickplace("end")
        self.msg_entry.delete(0, END)

# SET ALIAS


def infoloop(app):
    print("Infoloop")
    loop = asyncio.new_event_loop()
    while True:
        recv_msg = app.child_pipe_msg.recv()
        # print(app.gus.status)
        if recv_msg == 'OK':
            print("Recogiendo datos")
            loop.run_until_complete(app.communicate())
            print(app.gus.msg_r)
            app.parent_pipe.send(app.gus.msg_r)
            print("Cerrando recogida con status " + recv_msg)


GNUI = GPSNetworkUI


if __name__ == "__main__":
    # pdb.set_trace()
    gnui = GNUI()
    # try:
    loop = asyncio.new_event_loop()
    print(gnui.wk)
    pool = Pool(processes=gnui.wk)
    process = Process(target=TkEventLoop(gnui).mainloop)
    process.start()
    process2 = Process(target=infoloop, args=(gnui,))
    process2.start()
    process.join()
    process2.join()
    # TkEventLoop(gnui).mainloop()
    # except:
    # gnui.on_closing()
