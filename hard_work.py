from math import floor
import multiprocessing
import threading
import concurrent.futures


def workers(factor):
    """
    Define a number of workers for multiprocessing
    factor value mus be 1 to 100
    """
    nproc = multiprocessing.cpu_count()
    workers = floor(nproc * factor / 100)
    if workers <= 1:
        workers = 1
    return workers


def set_executor(workers):
    return concurrent.futures.ProcessPoolExecutor(workers)
