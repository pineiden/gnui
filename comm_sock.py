from Queue import Queue, Empty
import asyncio
import multiprocessing
from gnc_socket import GNCSocket


class CommGNCSocket(object):

    def __init__(self, async_queue, socket):
        if async_queue is not None:
            self.queue = async_queue
        else:
            self.queue = asyncio.Queue()
        if socket is not None:
            self.gus = socket
        else:
            self.gus = GNCSocket(mode='client')
    # ensure_future -> tasks -> gather
    async def infoloop(self):
        while True:
            try:
                gus_status = await self.queue.get_nowait()
                if gus_status == 'ON':
                    await self.gus.recv_msg()
                    await self.queue.put(self.gus.msg_r)
            except Empty:
                pass
